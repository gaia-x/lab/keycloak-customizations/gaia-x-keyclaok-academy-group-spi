package eu.gaiax.keycloak;

import org.junit.jupiter.api.Test;
import org.keycloak.events.Event;
import org.keycloak.events.EventType;
import org.keycloak.models.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static eu.gaiax.keycloak.GaiaXAcademyMapperProvider.ACADEMY_SAML;
import static eu.gaiax.keycloak.GaiaXAcademyMapperProvider.INCLUDE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GaiaXAcademyMapperProviderTest {

    @Test
    void initVars() {
        //Given a mapping.yaml exists
        //When I retrieve the mappings
        var instance = new GaiaXAcademyMapperProvider(null);
        //The map is defined and contains the roles and associated groups
        assertThat(instance.academyMappingFromRoles).isNotNull();
        assertThat(instance.academyMappingFromRoles.get("toto")).isNotNull();
        assertThat(instance.academyMappingFromRoles.get("toto").get(INCLUDE)).isNotNull().contains(12);
        assertThat(instance.academyGroups).isNotNull().containsExactlyInAnyOrder(2, 3, 4, 12);
    }

    @Test
    void getUserAcademyGroupsBasedOnHisRoles() {
        //Given a user logs in
        UserModel user = mock(UserModel.class);

        RealmModel realm = mock(RealmModel.class);

        KeycloakSession session = mock(KeycloakSession.class);
        KeycloakContext context = mock(KeycloakContext.class);
        when(session.getContext()).thenReturn(context);

        when(context.getRealm()).thenReturn(realm);

        RoleModel role1 = mock(RoleModel.class);
        RoleModel role2 = mock(RoleModel.class);
        when(role1.getName()).thenReturn("community");
        when(role2.getName()).thenReturn("toto");

        GroupModel group1 = mock(GroupModel.class);
        when(group1.getRoleMappingsStream()).thenReturn(Stream.of(role1));
        GroupModel group2 = mock(GroupModel.class);
        when(group2.getRoleMappingsStream()).thenReturn(Stream.of(role2));

        when(user.getGroupsStream()).thenReturn(Stream.of(group1, group2));

        var instance = new GaiaXAcademyMapperProvider(session);
        //When I retrieve its academy groups
        List<Map<String, List<Integer>>> userAcademyGroupsBasedOnHisRoles = instance.getUserAcademyGroupsBasedOnHisRoles(user);
        //Then I get the list with the proper information
        assertThat(userAcademyGroupsBasedOnHisRoles).isNotNull()
                .hasSize(2);
        List<Integer> includedGroups = userAcademyGroupsBasedOnHisRoles
                .stream()
                .flatMap(dto -> dto.get(INCLUDE).stream())
                .toList();
        assertThat(includedGroups)
                .containsExactlyInAnyOrder(4, 12);
    }

    @Test
    void onEvent() {
        var event = new Event();
        event.setUserId("userID");
        event.setRealmId("realmID");
        event.setType(EventType.LOGIN);
        event.setClientId(ACADEMY_SAML);
        UserModel user = mock(UserModel.class);
        when(user.getEmail()).thenReturn("ewann.gavard@gaia-x.eu");
        RoleModel role1 = mock(RoleModel.class);
        RoleModel role2 = mock(RoleModel.class);
        RoleModel role3 = mock(RoleModel.class);
        when(user.getRoleMappingsStream()).thenReturn(Stream.of(role1, role2, role3));
        when(role1.getName()).thenReturn("community");
        when(role2.getName()).thenReturn("all-staff");
        when(role3.getName()).thenReturn("members");

        KeycloakSession session = mock(KeycloakSession.class);
        KeycloakContext context = mock(KeycloakContext.class);
        when(session.getContext()).thenReturn(context);

        RealmModel realm = mock(RealmModel.class);
        when(context.getRealm()).thenReturn(realm);

        UserProvider userProvider = mock(UserProvider.class);
        when(session.users()).thenReturn(userProvider);
        when(userProvider.getUserById(realm, "userID")).thenReturn(user);

        var instance = new GaiaXAcademyMapperProvider(session);
        AcademyConfig.ACADEMY_REMOVE_FROM_GROUP_URL = "https://toto.com";
        AcademyConfig.ACADEMY_ADD_TO_GROUP_URL = "https://toto.com";
        instance.onEvent(event);
    }

    @Test
    void getAcademyGroupsFromMapping() {
        UserModel user = mock(UserModel.class);

        RealmModel realm = mock(RealmModel.class);

        KeycloakSession session = mock(KeycloakSession.class);
        KeycloakContext context = mock(KeycloakContext.class);
        when(session.getContext()).thenReturn(context);

        when(context.getRealm()).thenReturn(realm);

        RoleModel role1 = mock(RoleModel.class);
        RoleModel role2 = mock(RoleModel.class);
        RoleModel role3 = mock(RoleModel.class);
        when(role1.getName()).thenReturn("all-staff");
        when(role2.getName()).thenReturn("members");
        when(role3.getName()).thenReturn("stuff");

        GroupModel group1 = mock(GroupModel.class);
        when(group1.getRoleMappingsStream()).thenReturn(Stream.of(role1));
        GroupModel group2 = mock(GroupModel.class);
        when(group2.getRoleMappingsStream()).thenReturn(Stream.of(role2, role3));

        when(user.getGroupsStream()).thenReturn(Stream.of(group1, group2));

        var instance = new GaiaXAcademyMapperProvider(session);

        var userMapping = instance.getUserAcademyGroupsBasedOnHisRoles(user);
        var results = instance.getAcademyGroupsFromMapping(userMapping);

        assertThat(results).isNotNull().containsOnly(2);

    }
}