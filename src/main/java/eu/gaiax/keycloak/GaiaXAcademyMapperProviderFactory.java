package eu.gaiax.keycloak;

import org.keycloak.Config;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;

import java.util.List;

public class GaiaXAcademyMapperProviderFactory implements EventListenerProviderFactory {
    @Override
    public EventListenerProvider create(KeycloakSession session) {
        return new GaiaXAcademyMapperProvider(session);
    }

    @Override
    public void init(Config.Scope config) {
        //Nothing to do
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
        //Nothing to do
    }

    @Override
    public void close() {
        //Nothing to do
    }

    @Override
    public String getId() {
        return "GaiaXAcademyMapperProvider";
    }

    @Override
    public int order() {
        return EventListenerProviderFactory.super.order();
    }

    @Override
    public List<ProviderConfigProperty> getConfigMetadata() {
        return EventListenerProviderFactory.super.getConfigMetadata();
    }
}
