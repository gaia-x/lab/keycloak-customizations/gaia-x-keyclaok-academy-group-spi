package eu.gaiax.keycloak;

public class AcademyConfig {
    public static String ACADEMY_REMOVE_FROM_GROUP_URL = "https://academy.gaia-x.eu/moocit_admin_panel/api/groups/users/remove_group_user_associations/";
    public static String ACADEMY_ADD_TO_GROUP_URL = "https://academy.gaia-x.eu/moocit_admin_panel/api/groups/users/add_group_user_associations/";
}
