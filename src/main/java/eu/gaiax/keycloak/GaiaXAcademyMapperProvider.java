package eu.gaiax.keycloak;

import okhttp3.*;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleMapperModel;
import org.keycloak.models.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static eu.gaiax.keycloak.AcademyConfig.ACADEMY_ADD_TO_GROUP_URL;
import static eu.gaiax.keycloak.AcademyConfig.ACADEMY_REMOVE_FROM_GROUP_URL;

public class GaiaXAcademyMapperProvider implements EventListenerProvider {
    public static final String MAPPINGS_ENTRY_YAML = "mappings";
    public static final String GROUPS_ENTRY_YAML = "groups";
    public static final String ACADEMY_SAML = "academy-saml";
    public static final String INCLUDE = "include";
    public static final String EXCLUDE = "exclude";
    private final KeycloakSession session;
    private static final Logger LOGGER = LoggerFactory.getLogger(GaiaXAcademyMapperProvider.class);
    public final Map<String, Map<String, List<Integer>>> academyMappingFromRoles;
    public final List<Integer> academyGroups;

    public GaiaXAcademyMapperProvider(KeycloakSession session) {
        this.session = session;
        var config = this.readYaml();
        this.academyMappingFromRoles = (Map<String, Map<String, List<Integer>>>) config.getOrDefault(MAPPINGS_ENTRY_YAML, Map.of());
        this.academyGroups = (List<Integer>) config.getOrDefault(GROUPS_ENTRY_YAML, List.of());
    }

    @Override
    public void onEvent(Event event) {
        LOGGER.info("{} event", event.getClientId());
        if (EventType.LOGIN == event.getType() && event.getClientId().equalsIgnoreCase(ACADEMY_SAML)) {
            LOGGER.info("[userId:{}] Login or registration", event.getUserId());
            RealmModel realm = this.getRealm();
            UserModel user = this.getUser(event, realm);
            List<Map<String, List<Integer>>> usersAcademyGroups = getUserAcademyGroupsBasedOnHisRoles(user);
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info("[user:{} - groups:{}] Users academy groups retrieved", user.getId(), Arrays.toString(usersAcademyGroups.toArray()));
            }
            this.updateUserInAcademy(user, usersAcademyGroups);
        }
    }

    private RealmModel getRealm() {
        return this.session.getContext().getRealm();
    }

    public UserModel getUser(Event event, RealmModel realm) {
        return this.session.users().getUserById(realm, event.getUserId());
    }

    public Map<String, Object> readYaml() {
        try (InputStream io = this.getClass().getResourceAsStream("/mapping.yaml")) {
            Yaml yaml = new Yaml();
            return yaml.load(io);
        } catch (IOException e) {
            LOGGER.error("[e:{}] Unable to read the mapping.yaml config file. No mappings will be done", e.getMessage());
            return Map.of();
        }
    }

    public List<Map<String, List<Integer>>> getUserAcademyGroupsBasedOnHisRoles(UserModel user) {
        return user.getGroupsStream()
                .flatMap(RoleMapperModel::getRoleMappingsStream)
                .map(roleModel -> this.academyMappingFromRoles.getOrDefault(roleModel.getName(), Map.of(INCLUDE, List.of(), EXCLUDE, List.of())))
                .toList();
    }

    public void updateUserInAcademy(UserModel userModel, List<Map<String, List<Integer>>> groups) {
        String token = System.getenv("ACADEMY_TOKEN");
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        String removalRequestBody = """
                 {"group_user_associations": [
                         {
                             "group_ids": %s,
                             "user_emails": ["%s"]
                         }
                     ]
                 }
                """.formatted(this.academyGroups.toString(), userModel.getEmail());
        Request removalRequest = new Request.Builder().addHeader("Authorization", "Bearer " + token)
                .delete(RequestBody.create(MediaType.get("application/json"), removalRequestBody))
                .url(ACADEMY_REMOVE_FROM_GROUP_URL)
                .build();
        LOGGER.debug(removalRequestBody);
        try (Response response = client.newCall(removalRequest).execute()) {
            if (!response.isSuccessful()) {
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn("[user:{} - groups:{}] Unable to remove user's groups assignation", userModel.getId(), Arrays.toString(academyGroups.toArray()));
                }
                return;
            }
            LOGGER.info("removed user {} from all groups", userModel.getEmail());
        } catch (IOException ioException) {
            LOGGER.warn("Unable to remove user's groups assignation", ioException);
        }

        String updateRequestBody = """
                {"group_user_associations": [
                                {
                                    "group_ids": %s,
                                    "user_emails": ["%s"]
                                }
                            ]
                }
                """.formatted(getAcademyGroupsFromMapping(groups).toString(), userModel.getEmail());
        Request updateRequest = new Request.Builder().addHeader("Authorization", "Bearer " + token)
                .post(RequestBody.create(MediaType.get("application/json"), updateRequestBody))
                .url(ACADEMY_ADD_TO_GROUP_URL)
                .build();
        LOGGER.debug(updateRequestBody);
        try (Response response = client.newCall(updateRequest).execute()) {
            String userGroups = Arrays.toString(groups.toArray());
            if (!response.isSuccessful() && LOGGER.isWarnEnabled()) {
                LOGGER.warn("[user:{} - groups:{}] Unable to put user's groups assignation", userModel.getId(), userGroups);
            }
            LOGGER.info("added user {} to {}", userModel.getEmail(), userGroups);
        } catch (IOException ioException) {
            LOGGER.warn("Unable to put user's groups assignation", ioException);
        }
    }

    public List<Integer> getAcademyGroupsFromMapping(List<Map<String, List<Integer>>> groups) {
        List<Integer> finalList = new ArrayList<>();

        groups.forEach(groupDTO -> finalList.addAll(groupDTO.get(INCLUDE)));
        groups.forEach(groupDTO -> finalList.removeAll(groupDTO.get(EXCLUDE)));
        return finalList;
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
        // Nothing to do
    }

    @Override
    public void close() {
        // Nothing to do
    }
}
